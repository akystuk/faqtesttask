<?php 

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class FttEnum
{
    public const POST_TYPE = 'ftt_type';
    public const QUESTION_INPUT_NAME = 'ftt_question';
    public const ANSWER_INPUT_NAME = 'ftt_answer';
    public const ASSOCIATION_INPUT_NAME = 'ftt_association';

    /**
     * Return array with meta box names
     */
    public static function getInputNames()
    {
        return [
            self::QUESTION_INPUT_NAME, 
            self::ANSWER_INPUT_NAME, 
            self::ASSOCIATION_INPUT_NAME
        ];
    }
    
    /**
     * Array with post type paramrters
     */
    public static function getPostTypeData()
    {
        return array(
            'label' => null,
            'labels' => array(
                'name' => __('FAQ'),
                'singular_name' => __('FAQ'),
                'add_new' => __('Add new'),
                'add_new_item' => __('Create new FAQ'),
                'edit_item' => __('Edit FAQ'),
                'new_item' => __('New FAQ'),
                'view_item' => __('View'),
                'search_items' => __('Find FAQ'),
                'not_found' => __('FAQ not found'),
                'not_found_in_trash' => __('FAQ not found in trash'),
                'all_items' => __('All FAQ'),
                'menu_name' => __('FAQ')
            ),
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => false,
            'show_in_rest' => false,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-editor-help',
            'hierarchical' => false,
            'map_meta_cap'      => null,
            'supports' => array('title'),
            'taxonomies' => array(),
            'has_archive' => false,
            'rewrite' => true,
            'query_var' => true
        );
    }
}