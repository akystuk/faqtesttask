<?php
/**
 * Plugin Name: FAQ Test Task
 * Description: Plugin for show FAQ questions after posts
 * Version: 1.0
 * Author: Alex Yatsenko
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

define( 'FTT_PLUGIN_DIR_PATH', plugin_dir_path( __FILE__ ) );
require_once(FTT_PLUGIN_DIR_PATH . '/FttEnum.php');
require_once(FTT_PLUGIN_DIR_PATH . '/FttFields.php');
require_once(FTT_PLUGIN_DIR_PATH . '/FttFaqTestTask.php');
    
