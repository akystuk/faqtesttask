<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class FttFaqTestTask {
    
    /**
     * @var FttFaqTestTask The one instance.
     */
    private static $instance = null;

    private $fieldSevice = null;

    private $post = null;
    
    /**
     * The object is created from within the class itself
     * only if the class has no instance.
     */
    public static function instance() 
    {
        if ( null === self::$instance ) {
            self::$instance = new FttFaqTestTask();
        }

        return self::$instance;
    }
    
    /**
     * Initialize plugin.
     */
    public function __construct() 
    {
        $this->fieldSevice = new FttFields;
        $this->createPostType();
        if (
            (isset( $_GET['post_type']) && $_GET['post_type'] === FttEnum::POST_TYPE)
            || (isset( $_GET['post']) && get_post_type($_GET['post']) === FttEnum::POST_TYPE)
        ) {
            add_action('admin_init', array($this, 'enqueque'), 9999);
        }
        add_action('wp_enqueue_scripts', array($this, 'enquequeFaqStyle'));
        add_filter('the_content', array($this, 'ftt_add_faq_block'));
    }

    /**
     * Register css style for admin pages
     */
    public function enqueque()
    {
        wp_register_script('ftt-js', plugins_url('common.js', __FILE__), array('jquery'), '', true);
        wp_enqueue_script('ftt-js');
        wp_localize_script('ftt-js', 'faqRow', $this->fieldSevice->renderHtmlFields(true));

        wp_register_style('ftt-style', plugins_url('/libs/bootstrap.min.css', __FILE__));
        wp_enqueue_style('ftt-style');

        
        
    }

    /**
     * Register css style for front-end pages
     */
    public function enquequeFaqStyle()
    {
        if (is_singular('post')) {
            wp_register_style('ftt-style-public', plugins_url('/front/faq-front.css', __FILE__));
            wp_enqueue_style('ftt-style-public');
        }
    }
    
    /**
     * Create custom post type and add actions for meta boxes
     */
    public function createPostType()
    {
        add_action('init', function() {
            register_post_type(FttEnum::POST_TYPE, FttEnum::getPostTypeData());
        });

        add_filter( 'enter_title_here', function($text, $post){
            if ( $post->post_type === FttEnum::POST_TYPE ) {
                $text = __('Enter FAQ Name');
            }
            return $text;
        }, 10, 2 );

        add_action( 'add_meta_boxes', array(&$this, 'ftt_register_meta_boxes') );

        add_action( 'save_post', array(&$this, 'ftt_save_meta_box') );
    }

    /**
     * Register meta boxes.
     */
    public function ftt_register_meta_boxes() 
    {
        add_meta_box( 'ftt-1', __( 'FAQ', 'hcf' ), array(&$this, 'ftt_display_callback'), FttEnum::POST_TYPE );
    }

    /**
     * Meta box display callback.
     *
     * @param WP_Post $post Current post object.
     */
    public function ftt_display_callback( $post ) 
    {
        $questions = get_post_meta($post->ID, FttEnum::QUESTION_INPUT_NAME, true);
        $answers = get_post_meta($post->ID, FttEnum::ANSWER_INPUT_NAME, true);
        $association = get_post_meta($post->ID, FttEnum::ASSOCIATION_INPUT_NAME, true);
        echo $this->fieldSevice->renderHtmlAssociationPost($association);
        echo $this->fieldSevice->renderHtmlTableStart(); 
        if (
            (is_array($questions) && is_array($answers))
            && (count($questions) > 0 && count($answers) > 0)
        ) {
            for($i = 0; $i < count($questions); $i++){
                echo $this->fieldSevice->renderHtmlFields(true, $questions[$i], $answers[$i]);
            }
        }
        echo $this->fieldSevice->renderHtmlTableEnd(); 
        echo $this->fieldSevice->renderHtmlAddButton(); 
    }

    /**
     * Save meta box content.
     *
     * @param int $post_id Post ID
     */
    public function ftt_save_meta_box( $post_id ) 
    {
        if (get_post_type($post_id) === FttEnum::POST_TYPE) {
            $fields = FttEnum::getInputNames();
            foreach ($fields as $field) {
                if (array_key_exists($field, $_POST)) {
                    update_post_meta($post_id, $field,  $_POST[$field]);
                } else if ($field == FttEnum::ASSOCIATION_INPUT_NAME) {
                    update_post_meta($post_id, $field, '');
                } else {
                    update_post_meta($post_id, $field,  array());
                }
            }
        }
    }

    /**
     * Add FAQ Block after post content
     *
     * @param string $content Content
     */
    public function ftt_add_faq_block($content)
    {
        global $post;
        $args = array( 
            'post_type' => FttEnum::POST_TYPE,
            'meta_query' => [
                [
                    'key' => FttEnum::ASSOCIATION_INPUT_NAME,
                    'value' => $post->ID
                ]
            ]
        );

        $query = new WP_Query();
        $faq_posts = $query->query($args);
        $faq_post = null;

        if (count($faq_posts) > 0) {
            $faq_post = array_shift($faq_posts);
        }

        if (is_singular('post') && $faq_post !== null) {
            $questions = get_post_meta($faq_post->ID, FttEnum::QUESTION_INPUT_NAME, true);
            $answers = get_post_meta($faq_post->ID, FttEnum::ANSWER_INPUT_NAME, true);
            ob_start();
                include_once(FTT_PLUGIN_DIR_PATH . '/front/faq-front.php');
            $faq_html = ob_get_clean();
            $content = $content . $faq_html;
        }

        return $content;
    }
}
FttFaqTestTask::instance();