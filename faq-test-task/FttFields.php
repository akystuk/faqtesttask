<?php 

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class FttFields 
{
    /**
     * Render html for question and answer fields
     * 
     * @param bool $serialize Serialize html strig or not
     * @param string $question_value Question value
     * @param string $answer_value Answer value
     */
    public function renderHtmlFields($serialize = false, $question_value = '', $answer_value = '')
    {
        ob_start();?>
        <tr >
            <td class="md-3">
                <?php /* ?><label for="<?php echo FttEnum::QUESTION_INPUT_NAME;?>">Question</label> <?php */ ?>
                <input id="<?php echo FttEnum::QUESTION_INPUT_NAME;?>" type="text" name="<?php echo FttEnum::QUESTION_INPUT_NAME;?>[]" value="<?php echo $question_value;?>">
            </td>
            <td class="md-6">
                <?php /* ?><label for="<?php echo FttEnum::ANSWER_INPUT_NAME;?>">Answer</label><?php */ ?>
                <textarea cols="45" id="<?php echo FttEnum::ANSWER_INPUT_NAME;?>" type="date" name="<?php echo FttEnum::ANSWER_INPUT_NAME;?>[]"><?php echo $answer_value;?></textarea>
            </td>
            <td class="md-3">
                <button type="button" class="DeleteQ btn btn-outline-danger btn-sm" style="border-radius: 50%; min-width: 21px; padding: 0px; ">X</button>
            </td>
        </tr>
        <?php return $serialize ? ob_get_clean() : json_encode(ob_get_clean());
    }

    /**
     * Render first part of table with qwestions
     */
    public function renderHtmlTableStart()
    {
        ob_start();?>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th width="30%" scope="col">Question</th>
                    <th width="60%" scope="col">Answer</th>
                    <th width="10%" scope="col">Action</th>
                </tr>
                </thead>
                <tbody id="ftt_box">
        <?php return ob_get_clean();
    }

    /**
     * Render last part of table with qwestions
     */
    public function renderHtmlTableEnd()
    {
        ob_start();?>
            </tbody>
        </table>
        <?php return ob_get_clean();
    }

    /**
     * Render "Add button"
     */
    public function renderHtmlAddButton()
    {
        ob_start();?>
            <button type="button" id="AddQ" class="button button-primary button-me">Add Question</button>
        <?php return ob_get_clean();
    }

    /**
     * Render assocition input
     * 
     * @param string $association_value Association value
     */
    public function renderHtmlAssociationPost($association_value = '')
    {
        ob_start();?>
            <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Association</span>
                </div>
                <input id="<?php echo FttEnum::ASSOCIATION_INPUT_NAME;?>" type="text" name="<?php echo FttEnum::ASSOCIATION_INPUT_NAME;?>" value="<?php echo $association_value;?>">
            </div>
        <?php return ob_get_clean();
    }
}