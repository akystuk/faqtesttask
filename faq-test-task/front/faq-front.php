<?php 

if (
  (!isset($questions) && !isset($answers))
  || (!is_array($questions) && !is_array($answers))
) {
  exit;
}
?>
<div class="faq">
    <div class="faq__title__wrapper">
      <span class="faq__title__wrapper-before"></span>
      <h2 class="faq__title">
        <?php echo get_the_title($faq_post->ID); ?>
      </h2>
      <span class="faq__title__wrapper-after"></span>
    </div>
    <?php foreach ( $questions as $key => $question) {?>
      <div class="faq__questions">
        <div class="faq__question">
          <h3 class="faq__question-title">
            <span class="faq__question-img"></span>
            <?php echo $question;?>
          </h3>
          <div class="faq__question-answer">
            <p>
              <?php echo $answers[$key];?>
            </p>
          </div>
        </div>
      </div>
    <?php } ?>
</div>